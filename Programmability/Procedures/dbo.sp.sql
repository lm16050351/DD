﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE procedure [dbo].[sp] 
        @dbname sysname,
        @autofix nvarchar(4) = null
as

	set nocount on
	declare @ret_value int,
		@char_autofix varchar(25)
	declare @exec_stmt nvarchar(max)
		

	if (not (is_srvrolemember('sysadmin') = 1))  -- Make sure that it is the SA executing this.
	begin
		raiserror(15247,-1,-1)
		return(1)
	end

	-- sp_certify_removable does not allow outer transaction in the end
	set implicit_transactions off
	if (@@trancount > 0)
	begin
		raiserror(15002,-1,-1,'sys.sp_certify_removable')
		return (1)
	end

	select @autofix = lower(@autofix)

	if @autofix <> 'auto' and @autofix is not null
	begin
		raiserror(15255,-1,-1,@autofix)
		return(1)
	end

	if @dbname is null      -- Show usage diagram if no dbname supplied.
	begin
		raiserror(15256,-1,-1)
		return(1)
	end

	-- See if DB exists.
	if not exists (select * from master.dbo.sysdatabases where name = @dbname)
	begin
		raiserror(15010,-1,-1,@dbname)
		return(1)
	end

	-- Cannot take master, tempdb or model databases offline.
	if lower(@dbname) in ('master','tempdb','model')
	begin
		raiserror(15266,-1,-1,@dbname)
		return(1)
	end

	-- Will not be able to take db offline if user is in it.
	if @dbname = db_name()
	begin
		raiserror(15257,-1,-1)
		return(1)
	end

	-------------  Check things that exist only in the db.  -------------------
	select @char_autofix = (case
		when @autofix is not null then '''auto'''
		else                           'null'
		end)


	select @exec_stmt =
		'use ' + quotename(@dbname) + '

		declare @inx_ret_value int ,@int1 int
		select  @inx_ret_value = 1

		EXEC @inx_ret_value = sys.sp_check_removable ' + @char_autofix + '

		--Use @@rowcount for a user_assignable global variable for communication.
		if @inx_ret_value <> 0	--bad!!!!
			
		
		'
	EXEC (@exec_stmt)

	if @@rowcount > 0
		return (1)  -- Error was returned by other proc, so exit

	-- Take it offline
	raiserror('' ,0,1)
	-- construct the ALTER DATABASE command string
	select @exec_stmt = 'ALTER DATABASE ' + quotename(@dbname) +' SET OFFLINE WITH NO_WAIT'
	EXEC (@exec_stmt)

	return(0)	-- sp_certify_removable
GO